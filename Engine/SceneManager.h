#pragma once

#include <string>
#include <vector>

namespace Engine
{
	class SceneManager final
	{
		friend class Game;
		friend class Scene;

	public:
		static SceneManager* GetInstance();

		//Deletes all components and entities in the current scene.
		//Set the current scene with specified name and call OnLoad.
		void Load(Scene* scene);

		Scene* GetScene() const { return scene; }

	private:
		static SceneManager* instance;

		Scene* scene;

		SceneManager();
		~SceneManager();

		void Update(float dt);
		void Draw(float dt);
	};
}
