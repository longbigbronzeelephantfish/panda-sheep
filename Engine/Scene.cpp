#include "Entity.h"
#include "Scene.h"
#include "SceneManager.h"
#include "SystemManager.h"
#include "Component.h"

Engine::Scene::Scene()
{
	name = "";
}

Engine::Scene::~Scene()
{
	ClearEntities();
}

Engine::Entity* Engine::Scene::CreateEntity(std::string entityName)
{
	Entity* entity = new Entity(this, entityName);
	entities.push_back(entity);
	return entity;
}

void Engine::Scene::DestroyEntity(Entity* entity)
{
	for (size_t i = 0; i < entities.size(); ++i)
	{
		if (entities[i]->GetID() == entity->GetID())
		{
			entities.erase(entities.begin() + i);
			delete entity;
			break;
		}
	}
}

void Engine::Scene::ClearEntities()
{
	for (size_t i = 0; i < entities.size(); ++i)
		delete entities[i];

	entities.clear();
}

void Engine::Scene::Update(float dt)
{
	SystemManager::GetInstance()->Update(entities, dt);
}

void Engine::Scene::Draw(float dt)
{
	SystemManager::GetInstance()->Draw(entities, dt);
}
