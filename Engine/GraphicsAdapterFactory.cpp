#include "GraphicsAdapterFactory.h"

Engine::GraphicsAdapterFactory::GraphicsAdapterFactory()
{
	
}

Engine::GraphicsAdapterFactory::~GraphicsAdapterFactory()
{
	for (size_t i = 0; i < videoCards.size(); ++i)
		delete videoCards[i];

	if (factory)
	{
		factory->Release();
		factory = 0;
	}
}

bool Engine::GraphicsAdapterFactory::Initialize()
{
	if (FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory)))
		return false;

	//Store video cards
	int i = 0;

	do
	{
		GraphicsAdapter* videoCard = new GraphicsAdapter();

		if (!videoCard->Initialize(i, factory))
		{
			delete videoCard;
			break;
		}

		videoCards.push_back(videoCard);
	} while (++i);
}
