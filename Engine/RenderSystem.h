#pragma once

#include "System.h"

namespace Engine
{
	class RenderSystem final : public System
	{
		friend class SystemManager;

	public:
		static RenderSystem* GetInstance();

	protected:
		virtual void Update(const std::vector<Entity*>& entities, float dt) override;
		virtual void Draw(const std::vector<Entity*>& entities, float dt) override;

	private:
		static RenderSystem* instance;

		

		RenderSystem();
		~RenderSystem();
	};
};
