#pragma once

#include <d3d11.h>
#include "GraphicsAdapterFactory.h"

namespace Engine
{
	class GraphicsDevice final
	{
	public:
		GraphicsDevice();
		~GraphicsDevice();

		bool Initialize();

	private:
		ID3D11Device* device;
		ID3D11DeviceContext* deviceContext;
		IDXGISwapChain* swapchain;
		GraphicsAdapterFactory* factory;
	};
};
