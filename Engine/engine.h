#pragma once

#include "Debug.h"

//ECS
#include "Component.h"
#include "Entity.h"
#include "Scene.h"
#include "SceneManager.h"
#include "System.h"

//Components
#include "TransformComponent.h"
