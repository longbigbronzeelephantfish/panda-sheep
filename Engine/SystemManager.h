#pragma once

#include <vector>

namespace Engine
{
	class System;

	class SystemManager final
	{
		friend class Game;
		friend class Scene;
		friend class System;

	public:
		static SystemManager* GetInstance();

	private:
		static SystemManager* instance;

		std::vector<System*> systems;

		SystemManager();
		~SystemManager();

		//Add system
		void Add(System* system);

		//Remove specified system
		void Remove(System* system);

		//Clear and delete all systems
		void Clear();

		void Update(const std::vector<Entity*>& entities, float dt);
		void Draw(const std::vector<Entity*>& entities, float dt);
	};
};
