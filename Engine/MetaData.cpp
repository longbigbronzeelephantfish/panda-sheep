#include "Member.h"
#include "MetaData.h"

void Engine::MetaData::AddMember(Member member)
{ 
	members.push_back(member); 
}

const std::vector<Engine::Member>& Engine::MetaData::GetMembers()
{ 
	return members; 
}
