#pragma once

#include <vector>
#include "Object.h"

namespace Engine
{
	class Entity;

	class System : public Object
	{
		friend class SceneManager;
		friend class SystemManager;

	protected:
		System();
		virtual ~System();

		virtual void Update(const std::vector<Entity*>& entities, float dt) = 0;
		virtual void Draw(const std::vector<Entity*>& entities, float dt) = 0;
	};
};
