#pragma once

namespace Engine
{
	enum GameStatus
	{
		Stopped,
		Running,
		Paused,
	};
};
