#pragma once

#include <typeindex>
#include "MetaData.h"
#include "MetaManager.h"

namespace Engine
{
	template <typename MetaType>
	class MetaCreator
	{
	public:
		MetaCreator()
		{
			Initialize();
		}

		static MetaData* Get(void)
		{
			static MetaData instance;
			
			if (instance.GetName() == ""
				&& instance.GetSize() == 0)
				instance = MetaData(std::string(typeid(MetaType).name()), sizeof(MetaType));

			return &instance;
		}

		static MetaType* NullCast(void)
		{
			return reinterpret_cast<MetaType*>(NULL);
		}

	private:
		static void Initialize()
		{
			MetaData* data = Get();
			*data = MetaData(std::string(typeid(MetaType).name()), sizeof(MetaType));
			MetaManager::RegisterMeta(data);
		}
	};
};
