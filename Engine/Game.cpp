#include "Game.h"
#include "GameWindow.h"
#include "GameStatus.h"
#include "GameTime.h"
#include "SceneManager.h"
#include "SystemManager.h"

Engine::Game::Game()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
}

Engine::Game::~Game()
{
	delete SceneManager::GetInstance();
	delete SystemManager::GetInstance();
}

int Engine::Game::Run()
{
	MSG msg = {};
	GameTime time;
	float deltaTime;

	SceneManager* sceneManager = SceneManager::GetInstance();

	status = Running;

	Initialize();

	while (status != Stopped)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			deltaTime = 0;

			if (status == Running)
			{
				time.Update();
				deltaTime = time.GetDelta();
			}

			Update(deltaTime);
			sceneManager->Update(deltaTime);

			Draw(deltaTime);
			sceneManager->Draw(deltaTime);
		}
	}

	Shutdown();

	return (int)msg.wParam;
}
