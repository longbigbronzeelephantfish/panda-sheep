#include "engine.h"

static wchar_t availableClassName = 1;

static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
		{
		}
		break;
	case WM_PAINT:
		{
			Engine::GameWindow* window = (Engine::GameWindow*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

			if (window)
				window->OnPaint();
		}
		break;
	case WM_CHAR:
		{
			Engine::GameWindow* window = (Engine::GameWindow*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

			if (window)
				window->OnTextInput(static_cast<char>(wParam));
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

Engine::GameWindow::GameWindow()
	: GameWindow::GameWindow(0, 0, L"This is a game title", 0, 0, 0)
{
}

Engine::GameWindow::GameWindow(HINSTANCE hInstance, int nCmdShow, LPWSTR title, DWORD style, int iconResource, int smallIconResource)
	: className(availableClassName)
{
	//Set a empty window handle
	handle = 0;

	//Set a new available class name for new windows
	availableClassName++;

	//Create the window
	RegisterClassInstance(hInstance, iconResource, smallIconResource);
	InitialiseWindowInstance(hInstance, nCmdShow, title, style);

	//Register long ptr this game window class to window handle
	SetWindowLongPtr(handle, GWLP_USERDATA, (LONG)this);
}

void Engine::GameWindow::Destroy()
{
	DestroyWindow(handle);

}

void Engine::GameWindow::RegisterClassInstance(HINSTANCE hInstance, int iconResource, int smallIconResource)
{
	if (!hInstance)
		hInstance = GetModuleHandle(0);

	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = 0;
	wcex.lpszClassName = &className;

	if (smallIconResource)
		wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(smallIconResource));
	else
		wcex.hIconSm = 0;

	if (iconResource)
		wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(iconResource));
	else
		wcex.hIcon = 0;

	if (!RegisterClassExW(&wcex))
		MessageBox(0, L"Unable to register window class instance.", L"Error", MB_OK);
}

void Engine::GameWindow::InitialiseWindowInstance(HINSTANCE hInstance, int nCmdShow, LPWSTR title, DWORD style)
{
	if (!style)
		style = WS_OVERLAPPEDWINDOW & ~WS_MAXIMIZEBOX & ~WS_THICKFRAME;

	handle = CreateWindowW(&className, title, style,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!handle)
	{
		handle = 0;
		MessageBox(0, L"Unable to create game window handle.", L"Error", MB_OK);
		return;
	}

	if (!nCmdShow)
		nCmdShow = SW_SHOWNORMAL;

	ShowWindow(handle, nCmdShow);
	UpdateWindow(handle);
}
