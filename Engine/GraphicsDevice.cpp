#include "GraphicsDevice.h"

Engine::GraphicsDevice::GraphicsDevice()
{
	device = 0;
	deviceContext = 0;
	swapchain = 0;
	factory = 0;
}

Engine::GraphicsDevice::~GraphicsDevice()
{
	if (device)
	{
		device->Release();
		device = 0;
	}
		
	if (deviceContext)
	{
		deviceContext->Release();
		deviceContext = 0;
	}

	if (swapchain)
	{
		swapchain->Release();
		swapchain = 0;
	}
}

bool Engine::GraphicsDevice::Initialize()
{

}
