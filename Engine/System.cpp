#include "System.h"
#include "SystemManager.h"

Engine::System::System()
{
	SystemManager::GetInstance()->Add(this);
}

Engine::System::~System()
{
	SystemManager::GetInstance()->Remove(this);
}
