#pragma once

namespace Engine
{
	enum GameStatus;
	class GameWindow;

	class Game
	{
	public:
		Game();
		virtual ~Game();

		int Run();

		const GameWindow& GetWindow() { return window; }

	protected:
		virtual void Initialize() = 0;
		virtual void Shutdown() = 0;
		virtual void Update(float dt) = 0;
		virtual void Draw(float dt) = 0;

		const GameStatus GetStatus() { return status; }
		void Play() { status = Running; }
		void Stop() { status = Stopped; }
		void Resume() { status = Running; }
		void Pause() { status = Paused; }

	private:
		GameWindow window;
		GameStatus status;
	};
};
