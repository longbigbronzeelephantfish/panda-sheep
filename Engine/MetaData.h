#pragma once

#include <string>
#include <vector>
#include "Member.h"

namespace Engine
{
	class MetaData
	{
	public:
		MetaData() : name(""), size(0) {}
		MetaData(std::string name, unsigned size) : name(name), size(size) {}
		~MetaData() {}

		void AddMember(Member member);
		const std::vector<Member>& GetMembers();

		const std::string& GetName(void) const { return name; }
		unsigned GetSize(void) const { return size; }

	private:
		std::vector<Member> members;
		std::string name;
		unsigned size;
	};
};
