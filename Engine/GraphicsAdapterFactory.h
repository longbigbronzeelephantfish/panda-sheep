#pragma once

#include "GraphicsAdapter.h"

namespace Engine
{
	class GraphicsAdapterFactory final
	{
	public:
		GraphicsAdapterFactory();
		~GraphicsAdapterFactory();

		bool Initialize();

		const std::vector<GraphicsAdapter*>& GetAdapters() { return videoCards; }

	private:
		IDXGIFactory* factory;
		std::vector<GraphicsAdapter*> videoCards;
	};
};
