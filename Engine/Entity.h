#pragma once

#include <vector>
#include "Object.h"

namespace Engine
{
	class Component;
	class Scene;

	class Entity final : public Object
	{
		friend class Scene;

	public:
		//Destroy this entity
		void Destroy();

		//Set the parent of this entity
		void SetParent(Entity* parent);

		//Add child to this entity
		void AddChild(Entity* child);

		//Remove and delete specified child
		void RemoveChild(Entity* child);

		//Clears and delete all children
		void ClearChildren();

		//Returns parent
		Entity* GetParent() { return parent; }

		//Get all children
		const std::vector<Entity*>& GetChildren() { return children; }

		//Get the name
		const std::string& GetName() { return name; }

		//Set the name
		void SetName(std::string entityName) { name = entityName; }

		//Destroy component
		void DestroyComponent(Component* component);

		//Clear and delete all components
		void ClearComponents();

		//Create component
		template <typename ComponentType>
		ComponentType* CreateComponent()
		{
			ComponentType* component = new ComponentType();
			components.push_back((Component*)component);
			return component;
		}

		//Get component
		template <typename ComponentType>
		ComponentType* GetComponent()
		{
			for (size_t i = 0; i < components.size(); ++i)
			{
				ComponentType* component = dynamic_cast<ComponentType*>(components[i]);

				if (component)
				{
					return component;
				}
			}

			return 0;
		}

	private:
		Entity* parent;
		std::vector<Entity*> children;

		std::vector<Component*> components;

		std::string name;

		Scene* scene;

		//Initialise the entity
		Entity(Scene* scene, std::string name);

		//Remove this entity from the parent and delete all its children
		~Entity();
	};
};
