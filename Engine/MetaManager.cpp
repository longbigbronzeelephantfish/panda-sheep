#include "MetaData.h"
#include "MetaManager.h"

Engine::MetaMap Engine::MetaManager::metas;

void Engine::MetaManager::RegisterMeta(const MetaData* meta)
{
	//Look for meta with given name. Don't register if registered before.
	for (MetaMap::iterator it = metas.begin(); it != metas.end(); ++it)
		if (it->first == meta->GetName())
			return;

	//Register meta
	metas[meta->GetName()] = meta;
}

const Engine::MetaData* Engine::MetaManager::Get(std::string name)
{
	//Look for meta with given name
	for (MetaMap::iterator it = metas.begin(); it != metas.end(); ++it)
		if (it->first == name)
			return it->second;

	//Return NULL if not found
	return 0;
}
