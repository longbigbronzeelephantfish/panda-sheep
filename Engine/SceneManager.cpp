#include "Scene.h"
#include "SceneManager.h"

Engine::SceneManager* Engine::SceneManager::instance = 0;

Engine::SceneManager* Engine::SceneManager::GetInstance()
{
	if (!instance)
		instance = new SceneManager();

	return instance;
}

Engine::SceneManager::SceneManager()
{
	scene = 0;
}

Engine::SceneManager::~SceneManager()
{
	if (scene)
		delete scene;
}

void Engine::SceneManager::Load(Scene* scene)
{
	if (scene)
		delete scene;

	if (scene)
		this->scene = scene;
}

void Engine::SceneManager::Update(float dt)
{
	if (scene)
		scene->Update(dt);
}

void Engine::SceneManager::Draw(float dt)
{
	if (scene)
		scene->Draw(dt);
}
