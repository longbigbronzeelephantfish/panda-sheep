#pragma once

#include <Windows.h>

namespace Engine
{
	struct GameTime final
	{
		friend class Game;

	public:
		GameTime();

		const float GetDelta() { return delta; }

	private:
		LARGE_INTEGER frequency;
		LARGE_INTEGER total;
		LARGE_INTEGER previous;
		float delta;

		void Update();
	};
};
