#pragma once

#include <string>

namespace Engine
{
	class MetaData;

	class Member
	{
	public:
		Member(std::string name, unsigned offset, MetaData *meta) :
			name(name), offset(offset), meta(meta) {}
		~Member() {}

		const MetaData* GetMeta(void) const;

		const std::string& GetName(void) const { return name; }
		unsigned GetOffset(void) const { return offset; }

	private:
		std::string name;
		unsigned offset;
		const MetaData* meta;
	};
};
