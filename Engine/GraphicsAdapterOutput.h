#pragma once

#include <dxgi1_2.h>
#include <vector>

namespace Engine
{
	class GraphicsAdapterOutput final
	{
	public:
		GraphicsAdapterOutput();
		~GraphicsAdapterOutput();

		bool Initialize(UINT videoCardIndex, IDXGIAdapter* videoCard);

		const DXGI_MODE_DESC1* GetDisplayModeList() { return displayModeList; }
		const UINT& GetNumDisplayMode() { return numDisplayMode; }

	private:
		IDXGIOutput* monitor;
		DXGI_MODE_DESC1* displayModeList;
		UINT numDisplayMode;
	};
};
