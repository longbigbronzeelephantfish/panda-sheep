#pragma once

#include <DirectXMath.h>
#include "Component.h"

namespace Engine
{
	class TransformComponent final : public Component
	{
	public:
		//The local position
		DirectX::XMFLOAT3 position;

		//The local rotation by yaw pitch roll
		DirectX::XMFLOAT3 rotation;

		//The local scaling
		DirectX::XMFLOAT3 scaling;

		TransformComponent() 
		{
			position = DirectX::XMFLOAT3(0, 0, 0);
			rotation = DirectX::XMFLOAT3(0, 0, 0);
			scaling = DirectX::XMFLOAT3(1, 1, 1);
		}

		~TransformComponent() 
		{
		}
	};
}
