#include "System.h"
#include "SystemManager.h"

Engine::SystemManager* Engine::SystemManager::instance = 0;

Engine::SystemManager* Engine::SystemManager::GetInstance()
{
	if (!instance)
		instance = new SystemManager();

	return instance;
}

Engine::SystemManager::SystemManager()
{
}

Engine::SystemManager::~SystemManager()
{
	Clear();
}

void Engine::SystemManager::Add(System* system)
{
	systems.push_back(system);
}

void Engine::SystemManager::Remove(System* system)
{
	for (size_t i = 0; i < systems.size(); ++i)
	{
		if (systems[i]->GetID() == system->GetID())
		{
			systems.erase(systems.begin() + i);
			break;
		}
	}
}

void Engine::SystemManager::Clear()
{
	for (size_t i = 0; i < systems.size(); ++i)
		delete systems[i];

	systems.clear();
}

void Engine::SystemManager::Update(const std::vector<Entity*>& entities, float dt)
{
	for (size_t i = 0; i < systems.size(); ++i)
		systems[i]->Update(entities, dt);
}

void Engine::SystemManager::Draw(const std::vector<Entity*>& entities, float dt)
{
	for (size_t i = 0; i < systems.size(); ++i)
		systems[i]->Draw(entities, dt);
}
