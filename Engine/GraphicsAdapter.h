#pragma once

#include "GraphicsAdapterOutput.h"

namespace Engine
{
	class GraphicsAdapter final
	{
	public:
		GraphicsAdapter();
		~GraphicsAdapter();

		bool Initialize(UINT adapterIndex, IDXGIFactory* factory);

		const DXGI_ADAPTER_DESC& GetDescription() { return desc; }
		const std::vector<GraphicsAdapterOutput*> GetAdapterOutputs() { return monitors; }
		const size_t& GetDedicatedMemorySize() { return dedicatedMemorySize; }
		const char* GetName() { return name; }

	private:
		IDXGIAdapter* videoCard;
		DXGI_ADAPTER_DESC desc;
		std::vector<GraphicsAdapterOutput*> monitors;

		size_t dedicatedMemorySize;
		char name[128];
		size_t nameLength;
	};
};
