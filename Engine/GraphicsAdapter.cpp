#include "GraphicsAdapter.h"

Engine::GraphicsAdapter::GraphicsAdapter()
{
	//Get the video card at specified index
	videoCard = 0;
}

Engine::GraphicsAdapter::~GraphicsAdapter()
{
	for (size_t i = 0; i < monitors.size(); ++i)
		delete monitors[i];

	if (videoCard)
	{
		videoCard->Release();
		videoCard = 0;
	}
}

bool Engine::GraphicsAdapter::Initialize(UINT adapterIndex, IDXGIFactory* factory)
{
	if (factory->EnumAdapters(adapterIndex, &videoCard) == DXGI_ERROR_NOT_FOUND)
		return false;

	//Get video card description
	videoCard->GetDesc(&desc);

	//Get video card name
	if (!wcstombs_s(&nameLength, name, 128, desc.Description, 128))
		return false;

	//Get video card dedicated memory size
	dedicatedMemorySize = desc.DedicatedVideoMemory / 1024 / 1024;

	//Store monitors used by the video card
	int i = 0;

	do
	{
		GraphicsAdapterOutput* monitor = new GraphicsAdapterOutput();

		if (!monitor->Initialize(i, videoCard))
		{
			delete monitor;
			break;
		}

		monitors.push_back(monitor);
	} while (++i);
	
	return true;
}
