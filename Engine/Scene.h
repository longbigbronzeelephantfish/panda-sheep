#pragma once

#include <string>
#include <vector>

namespace Engine
{
	class Entity;

	class Scene
	{
		friend class SceneManager;

	public:
		//The name and identifier of the scene
		std::string name;

		//Create a entity into this scene
		Entity* CreateEntity(std::string name);

		//Remove specified entity in this scene
		void DestroyEntity(Entity* entity);

		//Clear all entity in this scene
		void ClearEntities();

		//Gets all entities
		const std::vector<Entity*>& GetEntities() { return entities; }

	protected:
		Scene();
		virtual ~Scene();

	private:
		std::vector<Entity*> entities;

		void Update(float dt);
		void Draw(float dt);
	};
};
