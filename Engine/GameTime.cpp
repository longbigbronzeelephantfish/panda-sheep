#include "GameTime.h"

Engine::GameTime::GameTime()
{
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&total);
}

void Engine::GameTime::Update()
{
	previous = total;
	QueryPerformanceCounter(&total);

	delta = (float)(total.QuadPart - previous.QuadPart);
	delta /= (float)frequency.QuadPart;
}
