#pragma once

#include "Object.h"

namespace Engine
{
	class Component : public Object
	{
		friend class Entity;

	protected:
		Component();
		virtual ~Component();
	};
}
