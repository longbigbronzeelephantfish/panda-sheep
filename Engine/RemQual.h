#pragma once

namespace Engine
{
	template <typename T>
	struct RemQual
	{
		typedef T type;
	};

	template <typename T>
	struct RemQual<const T>
	{
		typedef T type;
	};

	template <typename T>
	struct RemQual<T&>
	{
		typedef T type;
	};

	template <typename T>
	struct RemQual<const T&>
	{
		typedef T type;
	};

	template <typename T>
	struct RemQual<T&&>
	{
		typedef T type;
	};

	template <typename T>
	struct RemQual<T *>
	{
		typedef T type;
	};

	template <typename T>
	struct RemQual<const T *>
	{
		typedef T type;
	};
};
