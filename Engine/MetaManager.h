#pragma once

#include <map>

namespace Engine
{
	typedef std::map<std::string, const MetaData*> MetaMap;
	class MetaData;

	class MetaManager
	{
	public:
		//Register meta if it has not been registered before.
		static void RegisterMeta(const MetaData* meta);

		//Returns meta data with given name. Returns NULL if not found.
		static const MetaData* Get(std::string name);

	private:
		static MetaMap metas;
	};
};
