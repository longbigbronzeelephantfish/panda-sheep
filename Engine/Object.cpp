#include <objbase.h>
#include "Object.h"

Engine::Object::Object()
{
	GUID guid;
	CoCreateGuid(&guid);

	id += std::to_string(guid.Data1);
	id += std::to_string(guid.Data2);
	id += std::to_string(guid.Data3);
	id += reinterpret_cast<const char*>(guid.Data4);
}

Engine::Object::~Object()
{
}
