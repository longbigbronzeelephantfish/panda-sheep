#include "Entity.h"
#include "Scene.h"
#include "Component.h"

Engine::Entity::Entity(Scene* scene, std::string name)
{
	this->name = name;
	this->scene = scene;

	parent = 0;
}

Engine::Entity::~Entity()
{
	//Remove this entity from the parent
	if (parent)
	{
		for (size_t i = 0; i < parent->children.size(); ++i)
		{
			if (parent->children[i]->GetID() == GetID())
			{
				parent->children.erase(parent->children.begin() + i);
				break;
			}
		}
	}

	ClearChildren();

	ClearComponents();
}

void Engine::Entity::Destroy()
{
	scene->DestroyEntity(this);
}

void Engine::Entity::SetParent(Entity* newParent)
{
	//Remove this entity from the parent
	if (parent)
	{
		for (size_t i = 0; i < parent->children.size(); ++i)
		{
			if (children[i]->GetID() == GetID())
			{
				parent->children.erase(parent->children.begin() + i);
				break;
			}
		}
	}

	parent = newParent;
}

void Engine::Entity::AddChild(Entity* child)
{
	children.push_back(child);
}

void Engine::Entity::RemoveChild(Entity* child)
{
	for (size_t i = 0; i < children.size(); ++i)
	{
		if (children[i]->GetID() == child->GetID())
		{
			delete children[i];
			children.erase(children.begin() + i);
			break;
		}
	}
}

void Engine::Entity::ClearChildren()
{
	for (size_t i = 0; i < children.size(); ++i)
		delete children[i];

	children.clear();
}

void Engine::Entity::DestroyComponent(Component* component)
{
	for (size_t i = 0; i < components.size(); ++i)
	{
		if (components[i] == component)
		{
			components.erase(components.begin() + i);
			break;
		}
	}

	delete component;
}

void Engine::Entity::ClearComponents()
{
	for (size_t i = 0; i < components.size(); ++i)
		delete components[i];

	components.clear();
}
