#include "GraphicsAdapterOutput.h"

Engine::GraphicsAdapterOutput::GraphicsAdapterOutput()
{
	monitor = 0;
	displayModeList = 0;
}

Engine::GraphicsAdapterOutput::~GraphicsAdapterOutput()
{
	if (monitor)
	{
		monitor->Release();
		monitor = 0;
	}

	if (displayModeList)
	{
		delete[] displayModeList;
		displayModeList = 0;
	}
}

bool Engine::GraphicsAdapterOutput::Initialize(UINT videoCardIndex, IDXGIAdapter* videoCard)
{
	if (videoCard->EnumOutputs(videoCardIndex, &monitor) == DXGI_ERROR_NOT_FOUND)
		return false;

	IDXGIOutput1* monitor1;

	if (FAILED(monitor->QueryInterface(__uuidof(IDXGIOutput1), (void**)&monitor1)))
		return false;

	monitor1->GetDisplayModeList1(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED | DXGI_ENUM_MODES_STEREO, &numDisplayMode, 0);
	displayModeList = new DXGI_MODE_DESC1[numDisplayMode];
	monitor1->GetDisplayModeList1(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED | DXGI_ENUM_MODES_STEREO, &numDisplayMode, displayModeList);

	return true;
}
