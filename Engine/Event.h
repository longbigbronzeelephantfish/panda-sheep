#pragma once

#include "List.h"

namespace Engine
{
	//A event that has a empty parameter list
	struct VoidEvent final
	{
	public:
		void operator=(void(*func)())
		{
			functions.Clear();

			if (!func
				|| func == 0)
				return;

			functions.PushBack(func);
		}

		void operator-=(void(*func)())
		{
			for (int i = 0; i < functions.GetSize(); ++i)
			{
				if (functions[i] == func)
				{
					functions.RemoveAt(i);
					break;
				}
			}
		}

		void operator+=(void(*func)())
		{
			functions.PushBack(func);
		}

		void operator()()
		{
			for (int i = 0; i < functions.GetSize(); ++i)
				functions[i]();
		}

	private:
		List<void(*)()> functions;
	};

	//A event that takes in a single argument of specified type
	template <typename T>
	struct Event final
	{
	public:
		void operator=(void(*func)(T))
		{
			functions.Clear();

			if (!func
				|| func == 0)
				return;

			functions.PushBack(func);
		}

		void operator-=(void(*func)(T))
		{
			for (int i = 0; i < functions.GetSize(); ++i)
			{
				if (functions[i] == func)
				{
					functions.RemoveAt(i);
					break;
				}
			}
		}

		void operator+=(void(*func)(T))
		{
			functions.PushBack(func);
		}

		void operator()(T value)
		{
			for (int i = 0; i < functions.GetSize(); ++i)
			{
				functions[i](value);
			}
		}

	private:
		List<void(*)(T)> functions;
	};
}
