#include "RenderSystem.h"

Engine::RenderSystem* Engine::RenderSystem::instance = 0;

Engine::RenderSystem* Engine::RenderSystem::GetInstance()
{
	if (!instance)
		instance = new RenderSystem();

	return instance;
}

Engine::RenderSystem::RenderSystem()
{

}

Engine::RenderSystem::~RenderSystem()
{
	instance = 0;
}

void Engine::RenderSystem::Update(const std::vector<Entity*>& entities, float dt)
{
}

void Engine::RenderSystem::Draw(const std::vector<Entity*>& entities, float dt)
{
}
