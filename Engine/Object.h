#pragma once

#include <string>

namespace Engine
{
	class Object
	{
	public:
		Object();
		virtual ~Object();

		std::string GetID() { return id; }

	private:
		std::string id;
	};
}
