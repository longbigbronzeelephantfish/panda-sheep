#pragma once

#include <Windows.h>
#include "Event.h"

namespace Engine
{
	struct GameWindow final
	{
	public:
		VoidEvent OnPaint;
		Event<RECT> OnClientSizeChanged;
		Event<char> OnTextInput;

		//Initialise a game window with the default hInstance, and default nCmdShow
		GameWindow();

		//Initialise a game window with specified values. Set 0 for the arguments to make it the default value.
		GameWindow(HINSTANCE hInstance, int nCmdShow, LPWSTR title, DWORD style, int iconResource, int smallIconResource);

		void Destroy();

		HWND GetHandle() { return handle; }

	private:
		const wchar_t className;
		HWND handle;

		void RegisterClassInstance(HINSTANCE hInstance, int iconResource, int smallIconResource);
		void InitialiseWindowInstance(HINSTANCE hInstance, int nCmdShow, LPWSTR title, DWORD style);
	};
};
